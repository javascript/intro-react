# Introduction à React

Ce dépôt contient le code utilisé en exemple dans le cours [https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/html/react.html](https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/html/react.html) et [https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/html/react-2.html](https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/html/react-2.html).

Plusieurs versions progressives du dépôt pour accompagner le cours.


## Mise en place
 * récupérer le dépôt  
 * installer les paquets *Node.js* :
  ```bash
  $  npm install
  ```
  * démarrer le serveur de développement de *Webpack*
  ```bash
  $  npm run dev-server
  ```

## Les versions

Il faut charger une version avec

```bash
$ git checkout **tag**
```
où `tag` peut prendre comme valeur :

* `v0` : premier exemple basique avec *React*  
   voir `/src/scripts/main.js` et le point d'insertion `#insertReactHere` dans `/src/index.html`
* `v0.5` : second exemple avec création d'un emboitement d'éléments  
* `v1` : premier exemple avec la syntaxe JSX  
* `v2` : premiers composants sans état
  voir `/src/components/first.jsx`, `/src/components/second.jsx` et son utilisation dans `/src/mains.js`
* `v3` : utilisation des propriétés  
  voir `/src/components/person.jsx`, `/src/components/personListing.jsx` et son utilisation dans `/src/mains.js`
* `v3.1` : utilisation  de la syntaxe de pattern matching sur les objets pour destructurer l'objet `props` dans le paramètre du constructeur du composant.
* `v3.2` : la propriété `children`  
* `v4` : définition des composants à l'aide de classes   
  voir `/src/components/person_class.jsx`, `/src/components/personListing_class.jsx` et son utilisation dans `/src/mains.js`
* `v4.5` : définitions locales et valeurs par défaut  
   voir `/src/components/person.jsx` et son utilisation dans `/src/mains.js`
* `v4.6-problem` : génération d'une liste de composants, à l'aide de `map()`  
  voir `/src/components/personListing.jsx`    
    /!\\ mais **cette version pose problème** car il manque la gestion de la propriété `key` : voir dans la console  
* `v4.6` : génération d'une liste de composants avec gestion de la propriété `key`   
* `v5` : premier composant à état  
  voir `/src/components/person.jsx` 
* `v5.1` : gestion d'événements  
  voir `/src/components/star.jsx` et `onClick`
* `v5.2` : modification  et état courant : mise en évidence du problème  
  voir `/src/components/star.jsx`
* `v5.3` : modification  et état courant : utilisation d'une fonction en argument de `setOn` pour modifier l'état à partir de la valeur précédente    
  voir `/src/components/star.jsx` (et `/src/components/person.component.jsx`)
* `v5.5` : cycle de vie du composant : utilisation du hook `useEffect` pour agir au moment de la création du composant : juste après le premier rendu.  
  voir `/src/components/personListing.component.jsx` ainsi que  `/src/components/person.component.jsx` 
* `v 5.6` : utilisation des dépendances du hook `useEffect` pour déclencher son effet  
  voir `/src/components/person.component.jsx` 
* `v5.7` : utilisation du hook `useRef`  et complément dépendances de `useEffect`  
  voir `/src/components/person.component.jsx` 
* `v5.8` : mise en oeuvre de la fonction de nettoyage du `useEffect`  
  voir `/src/components/person.component.jsx` 
* `v5.9` : mise en oeuvre de la fonction de nettoyage du `useEffect`, appel à chaque exécution du hook   
  voir `/src/components/person.component.jsx` 
* `v6` : il faut remonter l'état et inversion du flux  
  voir `/src/components/delayButton.component.jsx` et `/src/components/personListingController.component.jsx`
* `v6.1` : restructuration des composants, suite de "placer l'état au plus haut" et de l'inversion du flux  
  voir `/src/components/personListingControls.component.jsx` et `/src/components/personListingController.component.jsx`  
* `v6.2` : refactorisation des composants, on continue à "remonter l'état"  
   voir `/src/components/app.jsx`, `/src/components/personListingData.jsx`  
* `v7` : exemple de composant avec des élément *non contrôlés* par *React*  
  voir `/src/components/addPerson.jsx`  
* `v7.1` : exemple de composant contrôlé   
  voir `/src/components/personListingData.jsx`  

Faire ```git checkout main``` pour revenir à la version finale.


