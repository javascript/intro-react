import '../assets/style/personControls.css';

import DelayButton from './delayButton.component.jsx';
import AddPerson from './addPerson.component.jsx';

const PersonListingControls = ( { started, closed, changeDelay, closeAction, startStop, addPerson } ) => {

    const buildDelayButtons = () => {
        const delayButtons = [250,1000,3000].map ( delay => <DelayButton delay = { delay }
                                                                         changeDelay = { changeDelay }
                                                                         key = { delay }/>);
        return delayButtons;
    }
    
      
    return(
        <div className = 'controls'>
            <button onClick= { startStop }>
                { started ? 'Stop'  : 'Start' }
            </button>
            { buildDelayButtons() }
            <button onClick= { closeAction }>
                { closed ? 'Open'  : 'Close' }
            </button>
            <AddPerson
                addPerson={ addPerson }
            />
        </div>
    );    
}

export default PersonListingControls;