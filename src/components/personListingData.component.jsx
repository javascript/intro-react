import { useState } from 'react';

import '../assets/style/personListingData.css';

import Data from './data.component.jsx';

const INITIAL_MIN_AGE_VALUE = '18';
const PersonListingData = ( { persons } ) => {

    const [minAge, setMinAge] = useState(INITIAL_MIN_AGE_VALUE);

    const nbPersonsOlderThan = minAge => {
        return persons.filter( person => person.age > minAge ).length ;
    }
    
    const ageList = () => {
        return persons.map( person => person.age );
    }

    const handleChangeAge = event => {
        setMinAge( event.target.value );
    }

    return(
        <div className="data">
            <div>âge min : <input type="number" className="ageMin"
                                  value= { minAge }
                                  onChange={ handleChangeAge }
                           />
            </div>
            <Data label="nb personnes" value={ persons.length } />
            <Data label={`âge > ${minAge}`} value={ nbPersonsOlderThan(minAge) } />
            <Data label="âge du plus jeune&nbsp;" value={ Math.min(...ageList()) } />
            <Data label="âge du plus vieux&nbsp;" value={ Math.max(...ageList()) } />
        </div>
    );
}

export default PersonListingData;