import { useState, useEffect } from 'react';

import '../assets/style/app.css';

import PersonListingController from './personListingController.component.jsx';
import PersonListingData from './personListingData.component.jsx';

import { mockFetch } from '../util/mockAPI.js';

const App = () => {

    const [persons, setPersons ] = useState([]);

    const fetchData = async () => {
        const data = await mockFetch('http://source.of.data/persons',10);
        setPersons( data );
    }      

    useEffect( () => {
    fetchData();            
    }, []);

    /* increment age of person with given id, state is updated */
    const incrementAge = (personId) => {
        setPersons ( previousPersons => {
                                            const olderPerson = previousPersons.find( person => person.id === personId );
                                            olderPerson.age = olderPerson.age + 1;
                                            return [...previousPersons];
                                        });
    }

    const addPerson = person => {
        const newId = `A${persons.length + 1}`;
        const newPerson = { ...person, id: newId };
        setPersons(previousPersons => [...previousPersons, newPerson] );
    }

    return(
        <div id="app">
            <PersonListingController
                persons = { persons }
                incrementAge = { incrementAge }
                addPerson = { addPerson }
            />
            <PersonListingData
                persons = { persons }
            />
        </div>
    );
}

export default App;