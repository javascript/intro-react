import { useRef } from 'react';

import '../assets/style/addPerson.css';

const DEFAULT_NAME = 'Anonymous';
const DEFAULT_AGE = 12;

const AddPerson = ( { addPerson } ) => {

    const nameInputRef = useRef(null);
    const ageInputRef = useRef(null);

    const handleClick = () => {
        const person = {
           name: nameInputRef.current.value || DEFAULT_NAME,
           age: parseInt(ageInputRef.current.value) || DEFAULT_AGE
        };
        addPerson(person);
        clearFields();
     }
  
    const clearFields = () => {
        nameInputRef.current.value = '';
        ageInputRef.current.value = '';
    }

     return (
        <div className="addPerson">
           <input
              type="text" placeholder="Name"
              ref={nameInputRef}
           />
           <input
              type="number" min="0" placeholder="Age"
              ref={ageInputRef}
           />
           <button onClick={handleClick}>Add</button>
        </div>
     );
}

export default AddPerson;