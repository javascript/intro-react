import { useState, useEffect } from 'react';



import '../assets/style/personListing.css';

import Person from './person.component.jsx';


const PersonListing =  props  => {
   const  { persons, incrementAge, started, delay } = props;

   const personComponents = persons.map(person => <Person 
                                                      {...person}
                                                      {...props}                                                      
                                                      key={person.id}                                                       
                                                   />);

   return (
      <div className="personListing">
         People getting older every {delay} seconds
         {personComponents}
      </div>
   );
}
export default PersonListing;
