const persons = [
   {
      id: 'A001',
      name: "Timoleon",
      age: 12
   },
   {
      id: 'A002',
      name: "Bilbo Baggins",
      age: 111
   },
   {
      id: 'A003',
      name: "Frodo Baggins",
      age: 33
   }
];

export default persons;
