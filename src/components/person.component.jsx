import { useEffect, useRef } from 'react';

import '../assets/style/person.css';

const Person = ( { name = 'Anonymous', age, id, started, delay, incrementAge } ) => {
   
   const timer = useRef( undefined );

   useEffect( () => {   
         if (started) {
            timer.current = setInterval( () =>  incrementAge( id ), delay);
         }
         return () =>  clearInterval( timer.current );                   
      }, [started, delay] );

   return (
      <div className="person">Here is :
         <div>name : <span>{ name }</span> </div>
         <div>age  : <span>{ age } </span>  </div>
      </div>
   );
}
export default Person;
