import { createRoot } from 'react-dom/client';

import App from '../components/app.component.jsx';

   const bootstrapReact = () => {
      const root = createRoot(document.getElementById('insertReactHere')); 

      const component = <App />;

      root.render(component);
   }


window.addEventListener('DOMContentLoaded', bootstrapReact);
