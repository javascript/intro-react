import { useState } from 'react';

import starOn from '../assets/images/star_on.png';
import starOff from '../assets/images/star_off.png';

/*
 define component Star, at first Star is off, click on component light on the star using handleClick
*/
const Star = () => {
   const [ on, setOn ] = useState(false) ;

   /* onClick listener, turn the star on if not yet */
   const handleClick = () => setOn( previousOn => ! previousOn );

   return(
      <img src = { on ? starOn : starOff }
           onClick = { handleClick }
      />
   );

}

export default Star;
