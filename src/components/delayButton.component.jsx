import '../assets/style/changedelay.css';

const delayText = 'délai';

const DelayButton = ( { delay, changeDelay }) => {

    const handleClick = () => changeDelay(delay);

    return(
        <button className="changedelay"
                onClick= { handleClick }>
           { delayText } = { delay }
        </button>
    );
}

export default DelayButton;