
import '../assets/style/data.css';

const Data = ( { label, value }) => {

    return(
        <div>
          <span className= "label"> { label } </span>
          :
          <span className= "value"> { value } </span>
        </div>
    );

}

export default Data;