
// used to "simulate" data fetching from some URL, here personsData is returned
/*
@param timetout simulate fetching delay, in ms, choose some high value (e.g. 2000) to highlight asynchronous operation
*/
import personsData from '../data/personsData.js';
const mockFetch = async (mockURL, timeout = 10) => {
   return await new Promise(resolve => setTimeout(() => {
                                                         console.log(`simulate fetching data from ${mockURL}`);
                                                         resolve(personsData);
                                                        }, 
                                                  timeout));        // simulate a 'timeout' duration for fetching operation
}

export { mockFetch };