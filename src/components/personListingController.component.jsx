import { useState } from 'react';

import '../assets/style/displayController.css';

import PersonListing from './personListing.component.jsx';
import PersonListingControls from './personListingControls.component.jsx';

const INITIAL_DELAY= 1000;

const PersonListingController = ( props ) => {

    const { addPerson } = props; 

    const [ closed, setClosed ] = useState(false);
    const [ started, setStarted ] = useState(false);
    const [ delay, setDelay ] = useState(INITIAL_DELAY);

    const startStop = () => setStarted( previousStarted => ! previousStarted);

    const closeComponent = () =>  {
            setClosed(previousClosed => ! previousClosed);
            setStarted(false);
    }

    const changeDelay = newDelay => setDelay(newDelay);

    const buildPersonListing = () => {
        if (closed)
            return null;
        else
            return (<PersonListing
                        { ...props }   
                        delay={delay}
                        started={started}
                    />);
    }

    const buildListingControls = () => {
        return (<PersonListingControls
                    started={started}
                    closed={closed}
                    changeDelay={changeDelay}
                    closeAction={closeComponent}
                    startStop={startStop}
                    addPerson={addPerson}
                />);
    }

    return (
            <div className="controller">
                { buildListingControls() }
                { buildPersonListing() }
            </div>
        );

}

export default PersonListingController;