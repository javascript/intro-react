
/*
* define a stateless React component 
*/
const First =
          () => <article>
                  <h3>React <strong>First component</strong> in action, using JSX</h3>
                  <div>this div is defined by the <code>First</code> component, in an article element defined as the root of the component</div>
                </article>;


export default First;
