
// import some other component
import First from './first.jsx';

/* define a stateless React component 
 * It uses another defined component, here First.
 */
const Second =
   () =>
      <div>
         <h3>Second React component that uses another React component</h3>
         <p>below comes a <code>First</code> component : </p>
         <First />                                            
      </div>

export default Second;
